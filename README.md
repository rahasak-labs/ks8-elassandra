# ks8 elassandra

## elassandra service

```
kubectl create -f elassandra-service.yaml
kubectl get services
```


## elassandra pod

```
kubectl create -f elassandra-pod.yaml
kubectl get pods
```

## test cql/elastic 

```
# access cassandra
cqlsh 10.4.1.70 9042 --cqlversion 3.4.4

# access elastic
curl 10.4.1.70:9200
```
